# Unit 1 Modify

## Requirement
1. You should have a sprite of your choice in your project which is destroyed after the spacebar is pressed three times.

2. You should have the explosion prefab from the Standard Assets appear when the sprite is destroyed.
3. You should play an explosion sound effect when the sprite is destroyed.
When the sprite gets destroyed, use Debug.Log to show the message “Destroyed!”

## Modifications
1. Modifications (Choose 3 from these)
1. Track mouse input instead of space bar. 
1. Change the color and velocity of explosion particle system.
1. Use Debug.Log to print hit points.
1. Play a sound each time the hit points decrease.
1. Replace explosion effect with a different prefab.
1. When the “E” key is pressed, HP increases by 1 point.  


## Skills
1. GetComponent
2. variable declaration
3. if/else statements
4.  using ==
5.  input using Input.GetKeyDown with if statements in Update
6. int
7.  GameObject, AudioSource 
7. data types,
7.   \ + - * / operators,
7.   understanding and usage of Update function, Destroy() and Instantiate()
9. Debug.Log()
