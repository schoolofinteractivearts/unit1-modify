﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    // int means Integer (whole numbers). hp stands for hit points. We use this variable to keep track of how many clicks we've made
    public int hp;

    // GameObject is a class name, describing the explosionPrefab object. Prefab is the template we use to instantiate(make copy of).
    public GameObject explosionPrefab;

    // Mofidication #ClickSound 
    // GameObject is a class name, describing the clickSoundPrefab object. Prefab is the template we use to instantiate(make copy of).
    public GameObject clickSoundPrefab;

    // Assign the GameObject of Bomb;
    public GameObject bombObject;


    // Update is called once per frame
    void Update()
    {
        // If statement: anything between the curly brackets {} will only happen if the game met the condition
        // Here, the condition is Input.GetKeyDown(KeyCode.Space), which means that the key got pressed down.
        // Modification #1: Change the input key from space bar to mouse button: left click
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            // We make hp decrease by 1. Remember to initialize it on the inspector.
            hp -= 1;

            // Modification #HP: debug.log showing HP
            // Use Debug.Log to show string (sentences). We connect strings by using + mark. 
            // Plain text sentence are quoted by quote marks.
			// Here it will show "hp is " followed by the value of hp variable.
			// e.g. if hp is 3, this will show "hp is 3";
            // ToString() is a function, that will convert the integer to a string("sentence"). E.g 10 to "10".
			Debug.Log("hp is " + hp.ToString());

            // Mofidication #ClickSound 
            // Instantiate the prefab containing the AudioSource. Since the AudioSource has playOnAwake Checked, it should play the sound once it instantiated
            Instantiate(clickSoundPrefab);

            // If statement: anything between the curly brackets {} will only happen if the game met the condition
            if (hp == 0)
            {
				// Instantiate (make copy of) the template in the scene.
				Instantiate(explosionPrefab);

                // GetComponent is used to get the component for given game object. In this case, the gameobject with AudioSource on it is the same game object we attach the code on.
                // So we could default the gameobject prefix, simply use GetComponent<AudioSource>
                // After we got the component of AudioSource, we call the function named Play() from it.
                GetComponent<AudioSource>().Play();

                // Destroy the bomb game object
                // Destroy is a function / method, it requires an argument, meaning the thing the function wants to destroy. In this case, the target is the bomb.
                Destroy(bombObject);

                // Use Debug.Log to show string(sentences)
                Debug.Log("Destroyed!");
            }
        }


        // Modification #Press E
        // When we press E, it should be another if statement to deal with this action
        if (Input.GetKeyDown(KeyCode.E))
        {

            // We make hp increase by 1. Remember to initialize it on the inspector.
            hp += 1;

            // Modification #HP: debug.log showing HP
            // Use Debug.Log to show string (sentences). We connect strings by using + mark. 
            // Plain text sentence are quoted by quote marks.
            // Here it will show "hp is " followed by the value of hp variable.
            // e.g. if hp is 3, this will show "hp is 3";
            // ToString() is a function, that will convert the integer to a string("sentence"). E.g 10 to "10".
            Debug.Log("hp is " + hp.ToString());
        }

    }
}
